import $ from 'jquery';
window.jQuery = $;window.$ = $;

export default class View{
  
  constructor(){
    this.inputValue=document.getElementById('display');
    this.numbers=document.querySelectorAll('[data-num]');
    this.clearBtn=$('#clear');
    this.submitBtn=$('#btn-submit');
    this.container=$('#container');
    this.displayCoins=$('.show-notes');
    this.notes=$('#notes');
    this.coins=$('#coins');
    this.smallCoins=$('#small-coins');
    this.back=$('#back');
    this.amount=$("#amount");
    this.warning=$('#warning');
    this.withdrawl=$("#withdrawl-container");
    this.amountField=$("#show-amount");
  }
  
  initView(){
    this.withdrawl.hide();
    this.container.show();
    this.showFocus();
  }

  bindClickNumber(handler){
    this.numbers.forEach((num)=>{
      num.addEventListener('click',handler);
    });
  }

  bindOnSubmit(handler){
    this.submitBtn.on('click',handler);
  }
  bindOnClear(handler){
    this.clearBtn.on('click',handler);
  }

  bindOnBack(handler){
    this.back.on('click',handler);
  }
  
  showFocus(){
    this.inputValue.value='£'+' ';
    this.inputValue.focus();
    this.inputValue.keyDown= function(e){
      e.preventDefault();
    }
  }

  showNumber(value){
    this.inputValue.value= `£ ${value}`;
  }
  
  clearNumber(clearValue){
    this.inputValue.value= `£ ${clearValue}`; 
  }
  
  showWarning(){
    this.warning.show('slow').delay(2000).fadeOut(500);
  }

  showDiv(amount){
    console.log(amount);
    this.container.hide();
    this.withdrawl.show();
    this.amountField.empty().append('£ '+ amount);
  }

  resetAll(){
    this.notes.empty();
    this.coins.empty();
    this.smallCoins.empty();
    this.amountField.textContent='';
  }

  goBackView(){
    console.log(this);
    this.initView();
    this.resetAll();
  }

  renderNotes(value,amount){
    this.notes.prepend("<div class='list'>" 
                          +"<div class='note-icon'></div>"
                          + "<div class='align-amount'>"
                          +value +" x "+ amount+ 
                          "</div></div>");
  }

  renderCoins(value,amount){
    this.coins.prepend("<div class='list'>" 
                          +"<div class='coin-icon'></div>"
                          + "<div class='align-amount-coin'>"
                          +value +" x "+ amount
                          + "</div></div>");
  }

  renderSmallCoins(value,amount){
    this.smallCoins.prepend("<div class='list'>" 
                              +"<div class='small-coin-icon'></div>"
                              + "<div class='align-amount-coin'>"
                              +value +" x "+ amount
                              + "</div></div>");
  }
}
