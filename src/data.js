export default class Store{
  constructor(){
  	this.moneyTypes=[1000,500,200,100,50,20,10,5,2,1],
  	this.radius={
        '20':40,
        '10':20,
        '5':50,
        '2':30,
        '1':10
    },
    this.value='',
	  this.amount='',
    //this.result={},
	  this.warning=false;
}

setValue(e){
  this.value+=e.target.dataset.num;
}

setClearValue(){
  this.value=this.value.slice(0,-1);
}

setAmount(){
  if(this.value===""||/^0*$/.test(this.value)){
		this.warning=true;
  }else{
    this.amount =this.value;
    this.warning=false;
	  //this.getMultiplies(parseInt(this.amount,10));
  }
}

/*getMultiplies(value){
  let firstSmallerValue=this.moneyTypes.find((note)=>{
    return value>=note;
  });
  let mulValue=Math.floor(value/firstSmallerValue);
  let nextValue=value%firstSmallerValue;
  this.result[firstSmallerValue]=mulValue;
  if(nextValue!==0){
    this.getMultiplies(nextValue);
  } 
}*/
getMultiplies(value){
  let result={};
  this.moneyTypes.forEach((money)=>{
    let mulValue=Math.floor(value/money);
    if(mulValue>=1){
      result[money]=mulValue;
      value%=money;
    }
  }
);
  return result;
}

resetAll(){
  this.result={};
  this.amount='';
  this.value='';
}

}
