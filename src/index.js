"use strict";

import './main.scss';

import View from './template';
import Controller from './AtmController';
import Store from './data';

const view = new View();
const store = new Store();

const controller = new Controller(view,store);

window.onload =function() {
	controller.setView();
}