export default class Controller {
  constructor(view,store){
    this.view=view;
    this.store=store;
    view.bindClickNumber(this.displayNumber.bind(this));
    view.bindOnSubmit(this.validate.bind(this));
    view.bindOnClear(this.clearOne.bind(this));
    view.bindOnBack(this.resetData.bind(this));
}

setView(){
    this.view.initView();
}

displayNumber(e){
  this.store.setValue(e);
  this.view.showNumber(this.store.value);
}

clearOne(){
  this.store.setClearValue();
  this.view.clearNumber(this.store.value);
}

validate(){
  this.store.setAmount();
  if(this.store.warning){
    this.view.showWarning();
  }else{
    this.view.showDiv(this.store.amount);
    //this.displayAmount(this.store.result);
    this.displayAmount(this.store.getMultiplies(this.store.amount));
  }
}

resetData(){
  this.store.resetAll();
  this.view.goBackView();
}


displayAmount(obj){
  if(this.store.amount.length>=6){
      this.view.displayCoins.css({'font-size':'25px'});
  }else{
      this.view.displayCoins.css({'font-size':'40px'});
  }
  for(var key in obj){
    if(+key>=50){
      this.view.renderNotes(obj[key],key);
    }else if(this.store.radius[key]>20){
      this.view.renderCoins(obj[key],key);
    }else if(this.store.radius[key]<=20){
      this.view.renderSmallCoins(obj[key],key);
    }
  }
}
}