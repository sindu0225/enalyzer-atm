const webpack = require('webpack');
const path = require('path');


const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    context: path.resolve(__dirname, "src"),

  entry: './index.js',
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'bundle.js'
  },
  module: {
    loaders: [{ 
      test: /\.js$/, 
      exclude: /node_modules/, 
      loader: "babel-loader" 
    },
    {
        test: /\.(s*)css$/,
        use: [ 'style-loader', 'css-loader' ,'sass-loader']
    },
    { 
      test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, 
      loader: "url-loader?limit=10000&mimetype=application/font-woff"
    },
    {
     test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, 
        loader: "file-loader" 
      }
    ]
  },
  
  devServer: {
  inline:true,
    port: 8008
},

plugins: [new UglifyJSPlugin(),new HtmlWebpackPlugin({
  template: path.join(__dirname, 'build', 'index.html'),
})
]
};
